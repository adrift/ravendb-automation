param (
    [String]
    $databaseName,
    
    [String]
    $ravenDbServer = "http://ravendb",
    
    [String]
    $certThumbprint,
    
    [String]
    $replicationFactor,
    
    [Bool]
    $encrypted = "false"
)

$requestUri = "$ravenDbServer/admin/databases?name=$databaseName"

if ($replicationFactor) {
    $requestUri = "$requestUri&replicationFactor=$replicationFactor"
}

Invoke-WebRequest -Uri $requestUri
                  -Method "PUT" 
                  -Body "{`"DatabaseName`":`"$databaseName`",`"Settings`":{},`"Disabled`":false,`"Encrypted`":$encrypted,`"Topology`":{`"DynamicNodesDistribution`":true}}"
                  -CertificateThumbprint $certThumbprint